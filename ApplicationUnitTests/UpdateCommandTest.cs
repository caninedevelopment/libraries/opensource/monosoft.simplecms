﻿using Application.Resources.Fragments.Commands.Update;
using Domain.Entities;
using Domain.Repositories;
using Implementation;
using Moq;
using NUnit.Framework;
using Persistence.InMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using static Persistence.InMemory.SimpleCmsDbContext;

namespace ApplicationUnitTests
{
   public class UpdateCommandTest
    {
        [SetUp]
        public void Setup()
        {
        }
        public readonly FragmentRepository fragmentRepository;
        private Mock<FragmentRepository> mockFragmentReposatory;
        [Test]
        public void Execute_ShouldUpdateBasedById()
        {
            mockFragmentReposatory = new Mock<FragmentRepository>(MockBehavior.Loose);
            
            SimpleCmsDbContext dbContext = InitNewDb(new MachineTime());
            Fragment dbfragment = new Fragment
            {
                Id = 1,
                TaskId = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"),
                HasChanged = false,
                Body="w",
                Name="i am here",
            };
            dbContext.Fragments.AddRange(dbfragment);
            dbContext.SaveChanges();
            Fragment fragment = new Fragment
            {
                TaskId = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"),
                HasChanged = true,
                Body = "q",
                Name = "what",
            };
            mockFragmentReposatory.Setup(x => x.Update(fragment));

            var updateCommand = new Command(fragmentRepository);
            var updatedfragment = dbContext.Fragments.First(t => t.TaskId == new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"));
            Assert.IsTrue(updatedfragment != null && updatedfragment.Name == "what" && updatedfragment.HasChanged == true && updatedfragment.Body == "q");

        }
    }
}
