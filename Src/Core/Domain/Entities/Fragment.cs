﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Interfaces;
namespace Domain.Entities
{
   public class Fragment
    {
        public int Id { get; set; }
        public Guid TaskId { get; set; }

        public string Name { get; set; }

        public string Body { get; set; }

        public bool HasChanged { get; set; }
    }
}
