﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Interfaces;
namespace Domain.Entities
{
   public class Site
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid HeaderFragment { get; set; }
        public Guid FooterFragment { get; set; }
    }
}

