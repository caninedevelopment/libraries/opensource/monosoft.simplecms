﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Interfaces;
namespace Domain.Entities
{
   public class Page
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string MenuHtml { get; set; }
        
        public List<Common.Guid> Fragments { get; set; }

        public int SortOrder { get; set; }


    }
}
