﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
   public interface IFragmentRepository
    {
        void Create(Fragment fragment);

        void Delete(Fragment fragment);

        List<Fragment> Getall();

        void Update(Fragment fragment);
    }
}
