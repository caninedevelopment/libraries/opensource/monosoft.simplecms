﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface IPageRepository
    {
            void Create(Page page);

            void Delete(Page page);

            List<Page> Getall();

            void Update(Page page);
        
    }
}
