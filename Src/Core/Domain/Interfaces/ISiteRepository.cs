﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface ISiteRepository
    {
        Site Get(Guid id);

        void Update(Site site);
    }
}
