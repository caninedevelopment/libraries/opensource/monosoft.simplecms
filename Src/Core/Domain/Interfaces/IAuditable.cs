﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
   public interface IAuditable
    {
        DateTime Created { get; set; }

        DateTime? LastModified { get; set; }
    }
}
