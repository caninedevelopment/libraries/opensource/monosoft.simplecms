﻿using Common;
using Domain.Entities;
using Domain.Interfaces;
using Monosoft.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories
{
   public class PageRepository : IPageRepository
    {
        protected readonly IDateTime _dateTime;

        public PageRepository(IDateTime dateTime)
        {
            _dateTime = dateTime;
        }
        public virtual void Create(Page page)
        {
            if (page.Fragments == null || page.Name == null || page.Id == null || page.MenuHtml == null || page.SortOrder == 0)
            {
                throw new ValidationException("values can not be null");
            }
            throw new System.NotImplementedException();
        }
        public virtual void Delete(Page page)
        {
            if (page.Fragments == null || page.Name == null || page.Id == null || page.MenuHtml == null || page.SortOrder == 0)
            {
                throw new ValidationException("values can not be null");
            }
            throw new System.NotImplementedException();
        }
        public virtual List<Page> Getall()
        {
            throw new System.NotImplementedException();
        }
        public virtual void Update(Page page)
        {
            if (page.Fragments == null || page.Name == null || page.Id == null || page.MenuHtml == null || page.SortOrder == 0)
            {
                throw new ValidationException("values can not be null");
            }
            throw new System.NotImplementedException();
        }
    }
}
