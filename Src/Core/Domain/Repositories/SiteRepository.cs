﻿using Common;
using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces;
using Monosoft.Common.Exceptions;

namespace Domain.Repositories
{
   public class SiteRepository : ISiteRepository
    {
        protected readonly IDateTime _dateTime;
        
        public SiteRepository(IDateTime dateTime)
        {
            _dateTime = dateTime;
        }

        public virtual Site Get(System.Guid Id)
        {
            if (Id == null)
            {
                throw new ValidationException("Id");
            }
            throw new System.NotImplementedException();
        }
        public virtual void Update(Site site)
        {
            if (site.Id == null || site.Name == null || site.HeaderFragment == null || site.FooterFragment == null)
            {
                throw new ValidationException("values can not be null");
            }
            throw new System.NotImplementedException();
        }
    }
}
