﻿using Common;
using Domain.Entities;
using Domain.Interfaces;
using System.Collections.Generic;
using Monosoft.Common.Exceptions;
namespace Domain.Repositories
{
   public class FragmentRepository : IFragmentRepository
    {
        protected readonly IDateTime _dateTime;
        
        public FragmentRepository(IDateTime dateTime)
        {
            _dateTime = dateTime;
        }
        public virtual void Create(Fragment fragment)
        {
            if (fragment.Body == null || fragment.Name == null || fragment.Id == 0)
            {
                throw new ValidationException("values can not be null");
            }
            throw new System.NotImplementedException();
        }
        public virtual void Delete(Fragment fragment)
        {
            if (fragment.Body == null || fragment.Name == null || fragment.Id == 0)
            {
                throw new ValidationException("values can not be null");
            }
            throw new System.NotImplementedException();
        }
        public virtual List<Fragment> Getall()
        {
            throw new System.NotImplementedException();
        }
        public virtual void Update(Fragment fragment)
        {
            if (fragment.Body == null || fragment.Name == null || fragment.Id == 0)
            {
                throw new ValidationException("values can not be null");
            }
            throw new System.NotImplementedException();
        }
    }
}
