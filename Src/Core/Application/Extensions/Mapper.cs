﻿using Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Extensions
{
    public static class Mapper
    {
        public static TResult Map<TSource, TResult>(this TSource source) where TSource : new() where
            TResult : IMapFrom<TSource>
        {
            TResult result = (TResult)Activator.CreateInstance(typeof(TResult));
            result.MapFrom(source);
            return result;
        }
    }
}

