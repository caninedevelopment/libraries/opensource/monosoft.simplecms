namespace Application.Resources.Page.Commands.Delete
{
    using Application.Interfaces;
    using Domain.Interfaces;
    using Domain.Repositories;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System.Linq;

    public class Command : IProcedure<Request>
    {
        private readonly IPageRepository _pageRepository;
        public Command(IPageRepository _pageRepository)
        {
            this._pageRepository = _pageRepository;
        }

        public void Execute(Request input)
        {
            Domain.Entities.Page page = _pageRepository.Getall().Where(t => t.Id == input.Id).FirstOrDefault();
            if (page == null)
            {
                throw new ElementDoesNotExistException("page does not exist", input.Id.ToString());
            }
            _pageRepository.Delete(page);
        }
    }
}