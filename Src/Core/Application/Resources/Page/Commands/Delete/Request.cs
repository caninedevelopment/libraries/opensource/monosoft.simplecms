namespace Application.Resources.Page.Commands.Delete
{
    using System;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }

        public void Validate()
        {
            if (Id == default)
                throw new ValidationException("Id");
        }
    }
}