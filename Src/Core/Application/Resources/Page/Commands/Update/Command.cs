namespace Application.Resources.Page.Commands.Update
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System.Linq;

    public class Command : IProcedure<Request>
    {
        private readonly IPageRepository _pageRepository;
        public Command(IPageRepository _pageRepository)
        {
            this._pageRepository = _pageRepository;
        }

        public void Execute(Request input)
        {
            Domain.Entities.Page page = _pageRepository.Getall().Where(t => t.Id == input.Id).FirstOrDefault();
            if (page == null)
            {
                throw new ElementDoesNotExistException("page does not exist", input.Id.ToString());
            }
            page.Name = input.Name;
            page.Fragments = input.Fragments;
            page.MenuHtml = input.menuHtml;
            page.SortOrder = input.SortOrder;

            _pageRepository.Update(page);
        }
    }
}