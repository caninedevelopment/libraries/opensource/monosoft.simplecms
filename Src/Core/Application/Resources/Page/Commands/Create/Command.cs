namespace Application.Resources.Page.Commands.Create
{
    using Application.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Domain.Interfaces;
    using System.Linq;

    public class Command : IProcedure<Request>
    {
        private readonly IPageRepository _pageRepository;
        public Command(IPageRepository _pageRepository)
        {
            this._pageRepository = _pageRepository;
        }

        public void Execute(Request input)
        {
            Domain.Entities.Page page = _pageRepository.Getall().Where(t => t.Id == input.Id).FirstOrDefault();
            if (page != null)
            {
                throw new ElementAlreadyExistsException("page already exist", input.Id.ToString());

            }
            page = new Domain.Entities.Page { Id = input.Id, Fragments = input.Fragments, MenuHtml = input.menuHtml, Name = input.Name, SortOrder = input.SortOrder };
            _pageRepository.Create(page);
        }
    }
}