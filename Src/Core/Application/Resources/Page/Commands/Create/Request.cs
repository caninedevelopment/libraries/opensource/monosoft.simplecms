namespace Application.Resources.Page.Commands.Create
{
    using Monosoft.Common.Command.Interfaces;
    using System;
    using System.Collections.Generic;
    using Monosoft.Common.Exceptions;
    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string menuHtml { get; set; }

        public List<Common.Guid> Fragments { get; set; }

        public int SortOrder { get; set; }
        public void Validate()
        {
            if (Id == default)
                throw new ValidationException("Id");
            if (string.IsNullOrEmpty(Name))
                throw new ValidationException("Name");
            if (string.IsNullOrEmpty(menuHtml))
                throw new ValidationException("MenuHtml");
            if (Fragments.Count == 0)
                throw new ValidationException("Fragments");
            

            
        }
    }
}