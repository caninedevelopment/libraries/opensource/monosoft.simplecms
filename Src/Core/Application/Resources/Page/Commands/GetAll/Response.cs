namespace Application.Resources.Page.Commands.GetAll
{
    using Application.Extensions;
    using Application.Interfaces;
    using Application.Resources.DTO;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    public class Response : IDtoResponse, IMapFrom<List<Page>>
    {
        public IList<PageDto> Pages { get; set; }

        public void MapFrom(List<Page> source)
        {
            Pages = source.Select(page => page.Map<Page, PageDto>()).ToList();
        }
    }
}