namespace Application.Resources.Page.Commands.GetAll
{
    using Application.Extensions;
    using Domain.Entities;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    public class Command : IFunction<Response>
    {
        private readonly IPageRepository _pageRepository;
        public Command(IPageRepository _pageRepository)
        {
            this._pageRepository = _pageRepository;
        }

        public Response Execute()
        {

            return _pageRepository.Getall().Map<List<Page>, Response>();
        }
    }
}