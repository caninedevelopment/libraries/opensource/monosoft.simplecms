namespace Application.Resources.Fragments.Commands.GetAll
{
    using Application.Extensions;
    using Application.Interfaces;
    using Domain.Entities;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    public class Command : IFunction<Response>
    {
        private readonly IFragmentRepository _fragmentRepository;
        public Command(IFragmentRepository _fragmentRepository)
        {
            this._fragmentRepository = _fragmentRepository;
        }

        public Response Execute()
        {
            return _fragmentRepository.Getall().Map<List<Fragment>, Response>();
        }
    }
}