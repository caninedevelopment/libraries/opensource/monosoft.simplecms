namespace Application.Resources.Fragments.Commands.GetAll
{
    using Application.Extensions;
    using Application.Interfaces;
    using Application.Resources.DTO;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    public class Response : IDtoResponse, IMapFrom<List<Fragment>>
    {
        public IList<FragmentDto> Fragments { get; set; }
        public void MapFrom(List<Fragment> source)
        {
            Fragments = source.Select(fragment => fragment.Map<Fragment, FragmentDto>()).ToList();
        }
    }
}