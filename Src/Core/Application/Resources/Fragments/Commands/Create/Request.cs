namespace Application.Resources.Fragments.Commands.Create
{
    using Monosoft.Common.Command.Interfaces;
    using System;
    using Monosoft.Common.Exceptions;
    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Body { get; set; }

        public void Validate()
        {
            if (Id == default)
                throw new ValidationException("Id");
            if (string.IsNullOrEmpty(Name))
                throw new ValidationException("Name");
            if (string.IsNullOrEmpty(Body))
                throw new ValidationException("Body");
        }
    }
}