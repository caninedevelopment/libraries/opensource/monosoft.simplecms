namespace Application.Resources.Fragments.Commands.Create
{
    using Application.Extensions;
    using Application.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Domain.Repositories;
    using DTO;
    using System.Linq;
    using Domain.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly IFragmentRepository _fragmentRepository;
        
        public Command(IFragmentRepository _fragmentRepository)
        {
            this._fragmentRepository = _fragmentRepository;
        }

        public void Execute(Request input)
        {
            Domain.Entities.Fragment fragment = _fragmentRepository.Getall().Where(t => t.TaskId == input.Id).FirstOrDefault();
            if (fragment != null)
            {
                throw new ElementAlreadyExistsException("fragemt already exist", input.Id.ToString());

            }
            fragment = new Domain.Entities.Fragment {TaskId = input.Id, Body = input.Body, Name = input.Name };
            _fragmentRepository.Create(fragment);
        }
    }
}