namespace Application.Resources.Fragments.Commands.Delete
{
    using Monosoft.Common.Exceptions;
    using Monosoft.Common.Command.Interfaces;
    using System;
    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }

        public void Validate()
        {
            if (Id == default)
                throw new ValidationException("Id");
        }
    }
}