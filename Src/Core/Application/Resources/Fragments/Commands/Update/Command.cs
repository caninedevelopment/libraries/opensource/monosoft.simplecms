namespace Application.Resources.Fragments.Commands.Update
{
    using Application.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Domain.Repositories;
    using System.Linq;
    using Domain.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly IFragmentRepository _fragmentRepository;
        public Command(IFragmentRepository _fragmentRepository)
        {
            this._fragmentRepository = _fragmentRepository;
        }
        
        public void Execute(Request input)
        {
            Domain.Entities.Fragment fragment = _fragmentRepository.Getall().Where(t => t.TaskId == input.Id).FirstOrDefault();
            if (fragment == null)
            {
                throw new ElementDoesNotExistException("fragemt does not exist", input.Id.ToString());
            }
            fragment.Body = input.Body;
            fragment.Name = input.Name;
            _fragmentRepository.Update(fragment);
        }
    }
}