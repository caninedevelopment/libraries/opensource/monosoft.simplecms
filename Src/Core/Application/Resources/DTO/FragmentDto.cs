﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.Interfaces;
using Monosoft.Common.Command.Interfaces;

namespace Application.Resources.DTO
{
    public class Fragmentlist
    {
        public List<FragmentDto> Fragments { get; set; }
    }


    public class FragmentDto : IMapFrom<Domain.Entities.Fragment>, IDtoResponse
    {
        
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Body { get; set; }

        public void MapFrom(Domain.Entities.Fragment source)
        {
            Id = source.TaskId;
            Name = source.Name;
            Body = source.Body;
        }
    }

}

