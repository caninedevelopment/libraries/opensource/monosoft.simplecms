﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.Interfaces;
using Monosoft.Common.Command.Interfaces;

namespace Application.Resources.DTO
{
   public class SiteDto : IMapFrom<Domain.Entities.Site>,IDtoResponse
    {
        public string Name { get; set; }

        public System.Guid HeaderFragment { get; set; }
        public System.Guid FooterFragment { get; set; }

        public void MapFrom(Domain.Entities.Site source)
        {
            Name = source.Name;
            HeaderFragment = source.HeaderFragment;
            FooterFragment = source.FooterFragment;

        }
    }
}
