﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.Interfaces;
using Monosoft.Common.Command.Interfaces;
using Domain.Entities;

namespace Application.Resources.DTO
{
    public class Pagelist
    {
        public List<PageDto> Pages { get; set; }
    }
    
    public class PageDto : IMapFrom<Domain.Entities.Page>, IDtoResponse
    {
        public System.Guid Id { get; set; } 

        public string Name { get; set; }

        public string MenuHtml { get; set; }

        public List<Common.Guid> Fragments { get; set; }

        public int SortOrder { get; set; }

        public void MapFrom(Domain.Entities.Page source)
        {
            Id = source.Id;
            Name = source.Name;
            MenuHtml = source.MenuHtml;
            Fragments = source.Fragments;
            SortOrder = source.SortOrder;
        }

    }
}
