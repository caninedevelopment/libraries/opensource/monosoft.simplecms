namespace Application.Resources.Site.Commands.Get
{
    using Application.Extensions;
    using Domain.Entities;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Command : IFunction<Request, Response>
    {
        private readonly ISiteRepository _siteRepository;

        public Command(ISiteRepository _siteRepository)
        {
            this._siteRepository = _siteRepository;
        }

        public Response Execute(Request input)
        {
            var site = _siteRepository.Get(input.Id);
            if (site == null)
                throw new ElementDoesNotExistException("site does not exist", input.Id.ToString());
            return site.Map<Site, Response>();
        }
    }
}