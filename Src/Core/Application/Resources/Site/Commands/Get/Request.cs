namespace Application.Resources.Site.Commands.Get
{
    using Monosoft.Common.Exceptions;
    using Monosoft.Common.Command.Interfaces;
    using System;

    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }
        public void Validate()
        {
            if (Id == Guid.Empty)
                throw new ValidationException("Id");
        }
    }
}