namespace Application.Resources.Site.Commands.Get
{
    using Monosoft.Common.Command.Interfaces;
    using DTO;
    using Application.Extensions;
    using Application.Interfaces;
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Linq;
    public class Response : SiteDto, IDtoResponse, IMapFrom<Site>
    {

    }
}