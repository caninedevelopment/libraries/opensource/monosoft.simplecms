namespace Application.Resources.Site.Commands.Update
{
    using Monosoft.Common.Command.Interfaces;
    using System;
    using System.Collections.Generic;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid HeaderFragment { get; set; }
        public Guid FooterFragment { get; set; }
        public void Validate()
        {
            if (Id == default)
                throw new ValidationException("Id");
            if (string.IsNullOrEmpty(Name))
                throw new ValidationException("Name");
            if (HeaderFragment == default)
                throw new ValidationException("HeaderFragment");
            if (FooterFragment == default)
                throw new ValidationException("FooterFragment");
        }
    }
}