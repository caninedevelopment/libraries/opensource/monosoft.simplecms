namespace Application.Resources.Site.Commands.Update
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Command : IProcedure<Request>
    {
        private readonly ISiteRepository _siteRepository;
        public Command(ISiteRepository _siteRepository)
        {
            this._siteRepository = _siteRepository;
        }

        public void Execute(Request input)
        {
            Domain.Entities.Site site = _siteRepository.Get(input.Id);
            if (site == null)
            {
                throw new ElementDoesNotExistException("site does not exist", input.Id.ToString());
            }
            site.Name = input.Name;
            site.FooterFragment = input.FooterFragment;
            site.HeaderFragment = input.HeaderFragment;
            _siteRepository.Update(site);
        }
    }
}