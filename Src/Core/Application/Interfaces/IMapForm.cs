﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IMapFrom<in TSource> where TSource : new()
    {
        void MapFrom(TSource source);
    }
}
