namespace Infrastructure.Implementation
{
    using Common;
    using global::Implementation;
    using Microsoft.Extensions.DependencyInjection;
    public static class DependencyInjection
    {
        public static IServiceCollection AddImplementation(this IServiceCollection services)
        {
            services.AddTransient<IDateTime, MachineTime>();
            return services;
        }
    }
}