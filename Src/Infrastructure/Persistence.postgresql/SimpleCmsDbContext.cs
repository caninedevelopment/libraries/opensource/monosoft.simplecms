﻿using Application.Interfaces;
using Common;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Persistence.PostgreSQL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.PostgreSQL
{
    public class SimpleCmsDbContext : DbContext,ISimpleCmsDbContext
    {
        private readonly IDateTime dateTime;

        public SimpleCmsDbContext(DbContextOptions<SimpleCmsDbContext> options, IDateTime dateTime)
            : base(options)
        {
            this.dateTime = dateTime;
            this.Database.EnsureCreated();
            
        }
        public DbSet<Fragment> Fragments { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Site> Sites { get; set; }

        public FragmentRepository FragmentRepository { get; set; }


        public override int SaveChanges()
        {
            foreach (Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<IAuditable> entry in ChangeTracker.Entries<IAuditable>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = dateTime.Now;
                        entry.Entity.LastModified = dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModified = dateTime.Now;
                        break;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Page>().Property(t => t.Fragments).HasColumnType("jsonb");
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(SimpleCmsDbContext).Assembly);
        }
    }
}
