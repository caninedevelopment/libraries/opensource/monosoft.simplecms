﻿using Common;
using Domain.Entities;
using Monosoft.Common.Exceptions;
using Persistence.PostgreSQL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.PostgreSQL.Repositories
{
   public class FragmentRepository : Domain.Repositories.FragmentRepository
    {
        private readonly ISimpleCmsDbContext fragmentDbContext;

        public FragmentRepository(ISimpleCmsDbContext fragmentDbContext, IDateTime dateTime) : base(dateTime)
        {
            this.fragmentDbContext = fragmentDbContext;
        }
        public override void Create(Fragment fragment)
        {
            fragmentDbContext.Fragments.Add(fragment);
            fragmentDbContext.SaveChanges();

        }
        public override void Delete(Fragment fragment)
        {
            
            fragmentDbContext.Fragments.Remove(fragment); 
            fragmentDbContext.SaveChanges();
        }
        public override List<Fragment> Getall()
        {
            return fragmentDbContext.Fragments.ToList();
        }
        public override void Update(Fragment fragment)
        {
            fragmentDbContext.Fragments.Update(fragment);
            fragmentDbContext.SaveChanges();
        }
    }
}
