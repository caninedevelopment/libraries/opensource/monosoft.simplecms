﻿using Common;
using Domain.Entities;
using Persistence.PostgreSQL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.PostgreSQL.Repositories
{
    public class SiteRepository : Domain.Repositories.SiteRepository
    {
        private readonly ISimpleCmsDbContext siteDbContext;
        public SiteRepository(ISimpleCmsDbContext siteDbContext, IDateTime dateTime) : base(dateTime)
        {
            this.siteDbContext = siteDbContext;
        }
        public override Site Get(System.Guid Id)
        {
            return siteDbContext.Sites.Find(Id);
        }

        public override void Update(Site site)
        {
            siteDbContext.Sites.Update(site);
            siteDbContext.SaveChanges();
        }
    }
}
