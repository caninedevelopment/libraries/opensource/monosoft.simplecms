namespace Infrastructure.Persistence.postgresql
{
    using Application.Interfaces;
    using global::Persistence.PostgreSQL;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.EntityFrameworkCore;
    using System;
    using global::Persistence.PostgreSQL.Repositories;
    using global::Persistence.PostgreSQL.Interfaces;

    public static class DependencyInjection
    { 
        
        public static IServiceCollection AddpostgresqlPersistenceProvider(this IServiceCollection services, string dbServerHost, string dbUser, string dbPassword, int dbServerPort, string dbName)
        {
            services.AddDbContext<global::Persistence.PostgreSQL.SimpleCmsDbContext>(options => options.UseNpgsql($"Host={dbServerHost};User ID={dbUser};Password={dbPassword};Port={dbServerPort};Database={dbName};"));
            services.AddScoped((Func<IServiceProvider,ISimpleCmsDbContext>)(provider => provider.GetService<global::Persistence.PostgreSQL.SimpleCmsDbContext>()));

            services.AddScoped<Domain.Interfaces.IFragmentRepository, FragmentRepository>();
            services.AddScoped<Domain.Interfaces.IPageRepository, PageRepository>();
            services.AddScoped<Domain.Interfaces.ISiteRepository, SiteRepository>();
            return services;
        }
    }
}