﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.PostgreSQL.Interfaces
{
   public interface ISimpleCmsDbContext
    {
        DbSet<Fragment> Fragments { get; set; }

        DbSet<Page> Pages { get; set; }

        DbSet<Site> Sites { get; set; }
        int SaveChanges();

    }
}
