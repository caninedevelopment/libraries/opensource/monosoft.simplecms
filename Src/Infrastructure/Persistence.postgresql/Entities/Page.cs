﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Domain.Interfaces;
namespace Persistence.PostgreSQL.Entities
{
   public class Page : IAuditable
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string MenuHtml { get; set; }

       
        public List<Common.Guid> Fragments { get; set; }

        public int SortOrder { get; set; }

        public DateTime Created { get; set; }

        public DateTime? LastModified { get; set; }

    }
}
