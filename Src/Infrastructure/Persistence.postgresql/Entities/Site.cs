﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Interfaces;
namespace Persistence.PostgreSQL.Entities
{
   public class Site : IAuditable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid HeaderFragment { get; set; }
        public Guid FooterFragment { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LastModified { get; set; }
    }
}

