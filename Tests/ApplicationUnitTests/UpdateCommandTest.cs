﻿using Application.Resources.Fragments.Commands.Update;
using Domain.Entities;
using Implementation;
using Monosoft.Common.Exceptions;
using Moq;
using NUnit.Framework;
using Persistence.InMemory;
using Persistence.InMemory.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Persistence.InMemory.SimpleCmsDbContext;

namespace ApplicationUnitTests
{
   public class UpdateCommandTest
    {
        [SetUp]
        public void Setup()
        {
        }
        
        
        [Test]
        public void Execute_ShouldUpdateBasedById()
        {
            SimpleCmsDbContext dbContext = InitNewDb(new MachineTime());
            Mock<FragmentRepository> mockFragmentReposatory = new Mock<FragmentRepository>(MockBehavior.Loose, dbContext);

            var dbfragment = new List<Fragment>()
            {
                new Fragment(){
                Id = 1,
                TaskId = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"),
                Body = "u",
                Name="i am here",
                },
                new Fragment()
                {
                Id = 2,
                TaskId = new Guid("9d2b3045-56c6-431e-9830-8f07d2e6991c"),
                Body = "w",
                Name="i am there",
                }
            };
            dbContext.Fragments.AddRange(dbfragment);
            dbContext.SaveChanges();
            Fragment fragment = new Fragment
            {
                Id = 1,
                TaskId = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"),
                Name = "what",
                Body = "q",
            };
            
            var updateCommand = new Command(mockFragmentReposatory.Object);
            updateCommand.Execute(new Request() { Name = "what", Body = "q", Id = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf") });
            var updatedFragment = dbContext.Fragments.First(t => t.TaskId == new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"));
            var notUpdatedFragment = dbContext.Fragments.First(t => t.TaskId == new Guid("9d2b3045-56c6-431e-9830-8f07d2e6991c"));
            Assert.IsTrue(updatedFragment != null && updatedFragment.Name == "what" && updatedFragment.Body == "q");
            Assert.IsTrue(notUpdatedFragment != null && notUpdatedFragment.Name == "i am there" && notUpdatedFragment.Body == "w");

        }
        [Test]
        public void RequestValidation_ShouldFail_NoIdProvided()
        {
            var request = new Request() { Body="sad",Name="sda"};
            var ex = Assert.Throws<ValidationException>(() => request.Validate());
            Assert.IsTrue(ex.Message == "A valid value should be provided for the property Id");
        }
        [Test]
        public void RequestValidation_ShouldFail_NoBodyProvided()
        {
            var request = new Request() { Id = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"), Name = "sda" };
            var ex = Assert.Throws<ValidationException>(() => request.Validate());
            Assert.IsTrue(ex.Message == "A valid value should be provided for the property Body");
        }
        [Test]
        public void RequestValidation_ShouldFail_NoNameProvided()
        {
            var request = new Request() { Id = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"), Body = "sda" };
            var ex = Assert.Throws<ValidationException>(() => request.Validate());
            Assert.IsTrue(ex.Message == "A valid value should be provided for the property Name");
        }
    }
}
