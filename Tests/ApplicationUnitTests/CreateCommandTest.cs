﻿using Application.Resources.Fragments.Commands.Create;
using Domain.Entities;
using Implementation;
using Monosoft.Common.Exceptions;
using Moq;
using NUnit.Framework;
using Persistence.InMemory;
using Persistence.InMemory.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using static Persistence.InMemory.SimpleCmsDbContext;

namespace ApplicationUnitTests
{
   public class CreateCommandTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Execute_ShouldCreate()
        {
            SimpleCmsDbContext dbContext = InitNewDb(new MachineTime());
            Mock<FragmentRepository> mockFragmentReposatory = new Mock<FragmentRepository>(MockBehavior.Loose, dbContext);

            
            Fragment dbfragment = new Fragment
            {
                Id = 1,
                TaskId = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"),
                Body = "u",
                Name = "i am here",
            };
            dbContext.Fragments.AddRange(dbfragment);
            dbContext.SaveChanges();

            var createCommand = new Command(mockFragmentReposatory.Object);
            createCommand.Execute(new Request() { Body = "q", Name = "what", Id = new Guid("eb4ade79-3900-4557-a47f-7578a76ae71e") });
            var CreateFragment = dbContext.Fragments.First(t => t.TaskId == new Guid("eb4ade79-3900-4557-a47f-7578a76ae71e"));
            Assert.IsTrue(CreateFragment != null && CreateFragment.Name == "what" && CreateFragment.Body == "q");
        }
        [Test]
        public void RequestValidation_ShouldFail_NoIdProvided()
        {
            var request = new Request() { Body = "sad", Name = "sda" };
            var ex = Assert.Throws<ValidationException>(() => request.Validate());
            Assert.IsTrue(ex.Message == "A valid value should be provided for the property Id");
        }
        [Test]
        public void RequestValidation_ShouldFail_NoBodyProvided()
        {
            var request = new Request() { Id = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"), Name = "sda" };
            var ex = Assert.Throws<ValidationException>(() => request.Validate());
            Assert.IsTrue(ex.Message == "A valid value should be provided for the property Body");
        }
        [Test]
        public void RequestValidation_ShouldFail_NoNameProvided()
        {
            var request = new Request() { Id = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"), Body = "sda" };
            var ex = Assert.Throws<ValidationException>(() => request.Validate());
            Assert.IsTrue(ex.Message == "A valid value should be provided for the property Name");
        }
    }
}
