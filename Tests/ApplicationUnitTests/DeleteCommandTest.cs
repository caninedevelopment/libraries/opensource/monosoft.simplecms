﻿using Application.Resources.Fragments.Commands.Delete;
using Domain.Entities;
using Implementation;
using Monosoft.Common.Exceptions;
using Moq;
using NUnit.Framework;
using Persistence.InMemory;
using Persistence.InMemory.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using static Persistence.InMemory.SimpleCmsDbContext;

namespace ApplicationUnitTests
{
    public class DeleteCommandTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Execute_ShouldCreate()
        {
            SimpleCmsDbContext dbContext = InitNewDb(new MachineTime());
            Mock<FragmentRepository> mockFragmentReposatory = new Mock<FragmentRepository>(MockBehavior.Loose, dbContext);

            
            List<Fragment> dbfragment = new List<Fragment>
            {
                new Fragment{
                Id = 1,
                TaskId = new Guid("9a131e66-3681-4caf-8dd4-0c2dff8f2edf"),
                Body = "u",
                Name = "i am here",
                },
                new Fragment
            {
                Id = 2,
                TaskId = new Guid("eb4ade79-3900-4557-a47f-7578a76ae71e"),
                Name = "what",
                Body = "q", 
                }
            };
            dbContext.Fragments.AddRange(dbfragment);
            dbContext.SaveChanges();

            var deleteCommand = new Command(mockFragmentReposatory.Object);
            deleteCommand.Execute(new Request() { Id = new Guid("eb4ade79-3900-4557-a47f-7578a76ae71e") });
            var allfragments = dbContext.Fragments.Count();
            Assert.IsTrue(allfragments == 1);
        }
        [Test]
        public void RequestValidation_ShouldFail_NoIdProvided()
        {
            var request = new Request() {};
            var ex = Assert.Throws<ValidationException>(() => request.Validate());
            Assert.IsTrue(ex.Message == "A valid value should be provided for the property Id");
        }
    }
}
