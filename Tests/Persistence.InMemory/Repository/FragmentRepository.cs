﻿using Common;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.InMemory.Repository
{
   public class FragmentRepository : IFragmentRepository
    {
        private readonly SimpleCmsDbContext fragmentDbContext;

        public FragmentRepository(SimpleCmsDbContext fragmentDbContext)
        {
            this.fragmentDbContext = fragmentDbContext;
        }
        public void Create(Fragment fragment)
        {
            fragmentDbContext.Fragments.Add(fragment);
            fragmentDbContext.SaveChanges();

        }
        public void Delete(Fragment fragment)
        {

            fragmentDbContext.Fragments.Remove(fragment);
            fragmentDbContext.SaveChanges();
        }
        public List<Fragment> Getall()
        {
            return fragmentDbContext.Fragments.ToList();
        }
        public void Update(Fragment fragment)
        {
            fragmentDbContext.Fragments.Update(fragment);
            fragmentDbContext.SaveChanges();
        }
    }
}
