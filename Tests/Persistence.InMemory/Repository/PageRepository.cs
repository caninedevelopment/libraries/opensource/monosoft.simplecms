﻿using Common;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.InMemory.Repository
{
   public class PageRepository : Domain.Repositories.PageRepository
    {
        private readonly SimpleCmsDbContext pageDbContext;
        public PageRepository(SimpleCmsDbContext pageDbContext, IDateTime dateTime) : base(dateTime)
        {
            this.pageDbContext = pageDbContext;
        }
        public override void Create(Page page)
        {
            pageDbContext.Pages.Add(page);
            pageDbContext.SaveChanges();

        }
        public override void Delete(Page page)
        {
            pageDbContext.Pages.Remove(page);
            pageDbContext.SaveChanges();
        }
        public override List<Page> Getall()
        {
            return pageDbContext.Pages.ToList();
            
        }
        public override void Update(Page page)
        {
            pageDbContext.Pages.Update(page);
            pageDbContext.SaveChanges();
        }
    }
}
