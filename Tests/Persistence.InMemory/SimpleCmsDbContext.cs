﻿using Common;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;

namespace Persistence.InMemory
{
    public class SimpleCmsDbContext : DbContext
    {
        private readonly IDateTime dateTime;

        
        public SimpleCmsDbContext(DbContextOptions<SimpleCmsDbContext> options, IDateTime dateTime)
            : base(options)
        {
            this.dateTime = dateTime;
            this.Database.EnsureCreated();

        }
        public static SimpleCmsDbContext InitNewDb(IDateTime dateTime)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SimpleCmsDbContext>();
            optionsBuilder.UseInMemoryDatabase("simplecms");
            var context = new SimpleCmsDbContext(optionsBuilder.Options, dateTime);
            context.Database.EnsureDeleted();
            return context;
        }

        public DbSet<Fragment> Fragments { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Site> Sites { get; set; }

        

        public override int SaveChanges()
        {
            foreach (Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<IAuditable> entry in ChangeTracker.Entries<IAuditable>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = dateTime.Now;
                        entry.Entity.LastModified = dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModified = dateTime.Now;
                        break;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(SimpleCmsDbContext).Assembly);
        }
    }
}